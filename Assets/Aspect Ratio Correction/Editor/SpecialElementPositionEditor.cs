﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpecialElementPosition))]
public class SpecialElementPositionEditor : Editor
{
    SpecialElementPosition t;
    SerializedObject getTarget;
    SerializedProperty arrInstance;
    int arrSize;

    RectTransform rTransform;

    private void OnEnable()
    {
        t = (SpecialElementPosition)target;
        getTarget = new SerializedObject(t);
        arrInstance = getTarget.FindProperty("AspectRatioCorrections");

        rTransform = t.GetComponent<RectTransform>();
    }

    public override void OnInspectorGUI()
    {
        getTarget.Update();

        #region List Size Field

        arrSize = arrInstance.arraySize;
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox("Set list of multiple aspect ratio.", MessageType.None);
        EditorGUIUtility.labelWidth = 50;
        EditorGUIUtility.fieldWidth = 70;
        arrSize = EditorGUILayout.IntField("List Size", arrSize, GUILayout.MaxWidth(90), GUILayout.MinWidth(70));

        #endregion

        #region Add / Remove Element Button

        if (GUILayout.Button(new GUIContent("+"), GUILayout.MaxWidth(40), GUILayout.MinWidth(30)))
        {
            arrSize = Mathf.Clamp(arrSize + 1, 0, 200);
        }

        if (GUILayout.Button(new GUIContent("-"), GUILayout.MaxWidth(40), GUILayout.MinWidth(30)))
        {
            arrSize = Mathf.Clamp(arrSize - 1, 0, 200);
        }

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();

        #endregion

        GUILayout.Space(20);

        if (arrSize != arrInstance.arraySize)
        {
            while (arrSize > arrInstance.arraySize)
            {
                arrInstance.InsertArrayElementAtIndex(arrInstance.arraySize);
            }
            while (arrSize < arrInstance.arraySize)
            {
                arrInstance.DeleteArrayElementAtIndex(arrInstance.arraySize - 1);
            }
        }

        for (int i = 0; i < arrSize; i++)
        {
            SerializedProperty MyListRef = arrInstance.GetArrayElementAtIndex(i);

            SerializedProperty a = MyListRef.FindPropertyRelative("XPos");
            SerializedProperty b = MyListRef.FindPropertyRelative("YPos");
            SerializedProperty c = MyListRef.FindPropertyRelative("XSize");
            SerializedProperty d = MyListRef.FindPropertyRelative("YSize");
            SerializedProperty _ar = MyListRef.FindPropertyRelative("Resolution");

            #region Rect Transform Label

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("Pos X", GUILayout.MaxWidth(80), GUILayout.MinWidth(40));
            GUILayout.Space(20);
            EditorGUILayout.LabelField("Pos Y", GUILayout.MaxWidth(80), GUILayout.MinWidth(40));
            GUILayout.Space(20);
            EditorGUILayout.LabelField("Width", GUILayout.MaxWidth(80), GUILayout.MinWidth(40));
            GUILayout.Space(20);
            EditorGUILayout.LabelField("Height", GUILayout.MaxWidth(80), GUILayout.MinWidth(40));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            #endregion

            #region Rect Transform Field
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.PropertyField(a, GUIContent.none, GUILayout.MaxWidth(100), GUILayout.MinWidth(50));
            EditorGUILayout.PropertyField(b, GUIContent.none, GUILayout.MaxWidth(100), GUILayout.MinWidth(50));
            EditorGUILayout.PropertyField(c, GUIContent.none, GUILayout.MaxWidth(100), GUILayout.MinWidth(50));
            EditorGUILayout.PropertyField(d, GUIContent.none, GUILayout.MaxWidth(100), GUILayout.MinWidth(50));
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            #endregion

            #region Resolution Select Field

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("Select Resolution", GUILayout.MaxWidth(120), GUILayout.MinWidth(60));
            EditorGUILayout.PropertyField(_ar, GUIContent.none, GUILayout.MaxWidth(220), GUILayout.MinWidth(120));

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            #endregion

            EditorGUILayout.Space(5);

            #region Get RectTransform Button

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(new GUIContent("Get current RectTransform Position"), GUILayout.MaxWidth(220), GUILayout.MinWidth(80)))
            {
                a.floatValue = rTransform.anchoredPosition.x;
                b.floatValue = rTransform.anchoredPosition.y;
                c.floatValue = rTransform.sizeDelta.x;
                d.floatValue = rTransform.sizeDelta.y;
            }
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            #endregion

            EditorGUILayout.Space(5);
        }

        getTarget.ApplyModifiedProperties();
    }
}
