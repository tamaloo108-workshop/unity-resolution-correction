using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialElementPosition : MonoBehaviour
{
    private RectTransform targetRect;

    public AspectRatioElement[] AspectRatioCorrections = new AspectRatioElement[] { new AspectRatioElement(), new AspectRatioElement() };

    string _aspect;
    string _targetAspect;

    private void OnEnable()
    {
        targetRect = GetComponent<RectTransform>();

        _aspect = AspectRatio(Screen.width, Screen.height);

        for (int i = 0; i < AspectRatioCorrections.Length; i++)
        {
            _targetAspect = AspectRatioCorrections[i].GetTargetAspect();

            if (_aspect == _targetAspect)
            {
                targetRect.anchoredPosition = new Vector2(AspectRatioCorrections[i].XPos, AspectRatioCorrections[i].YPos);
                targetRect.sizeDelta = new Vector2(AspectRatioCorrections[i].XSize, AspectRatioCorrections[i].YSize);
                break;
            }
        }
    }

    public static string AspectRatio(int width, int height)
    {
        int r;
        int w = width;
        int h = height;
        int oa = w;
        int ob = h;
        while (h != 0)
        {
            r = w % h;
            w = h;
            h = r;
        }

        return (oa / w).ToString() + ":" + (ob / w).ToString();
    }
}
